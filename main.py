import tensorflow as tf
import numpy as np
from bach_dataset import BachDataset
from synthesizer import Synthesizer, SineOscillator


class BachGenerator(tf.keras.Model):

    def __init__(self, n_notes, n_embedding_dims=5, **kwargs):
        super().__init__(**kwargs)

        self.model = tf.keras.models.Sequential([
            tf.keras.layers.Embedding(input_dim=n_notes, output_dim=n_embedding_dims, input_shape=[None]),
            tf.keras.layers.Conv1D(32, kernel_size=2, padding="causal", activation="relu"),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv1D(48, kernel_size=2, padding="causal", activation="relu", dilation_rate=2),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv1D(64, kernel_size=2, padding="causal", activation="relu", dilation_rate=4),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv1D(96, kernel_size=2, padding="causal", activation="relu", dilation_rate=8),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.LSTM(256, return_sequences=True),
            tf.keras.layers.Dense(n_notes, activation="softmax")
        ])

    def call(self, inputs, **kwargs):
        return self.model.call(inputs, **kwargs)

    def build(self, input_shape):
        self.model.build(input_shape)
        super().build(input_shape)

    def get_config(self):
        return super().get_config()

    def generate_chorale(self, seed_chords, length):
        arpegio = BachDataset.preprocess(tf.constant(seed_chords, dtype=tf.int64))
        arpegio = tf.reshape(arpegio, [1, -1])
        for chord in range(length):
            for note in range(4):
                next_note = np.argmax(self.model.predict(arpegio), axis=-1)[:1, -1:]
                arpegio = tf.concat([arpegio, next_note], axis=1)
        arpegio = tf.where(arpegio == 0, arpegio, arpegio + dataset.min_note - 1)
        return tf.reshape(arpegio, shape=[-1, 4])

    def generate_chorale_v2(self, seed_chords, length, temperature=1):
        arpegio = BachDataset.preprocess(tf.constant(seed_chords, dtype=tf.int64))
        arpegio = tf.reshape(arpegio, [1, -1])
        for chord in range(length):
            for note in range(4):
                next_note_probas = self.model.predict(arpegio)[0, -1:]
                rescaled_logits = tf.math.log(next_note_probas) / temperature
                next_note = tf.random.categorical(rescaled_logits, num_samples=1)
                arpegio = tf.concat([arpegio, next_note], axis=1)
        arpegio = tf.where(arpegio == 0, arpegio, arpegio + dataset.min_note - 1)
        return tf.reshape(arpegio, shape=[-1, 4])


if __name__ == '__main__':
    dataset = BachDataset("bach_dataset")
    model = BachGenerator(dataset.n_notes)
    model.build([None, None])
    model.summary()

    optimizer = tf.keras.optimizers.Nadam(learning_rate=1e-3)
    model.compile(loss="sparse_categorical_crossentropy", optimizer=optimizer, metrics=["accuracy"])
    model.fit(dataset.train_set, epochs=30, validation_data=dataset.valid_set)

    new_chorale = model.generate_chorale(dataset.test_chorales[2][:8], 56)
    melodies = BachDataset.notes_to_frequencies(new_chorale).T

    osc = SineOscillator()
    synt = Synthesizer(osc, 44100)

    for melody in melodies:
        synt.add_voice(melody)

    synt.play_voices()
