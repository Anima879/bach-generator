# Bach generator

The purpose of this project is to create a infinite generator of bach chorale music.

There is two main parts :
* The synthesizer to generate sounds (create from scratch).
* The generator model based on Recurent Neural Network.
