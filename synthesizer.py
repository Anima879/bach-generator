from abc import ABC, abstractmethod
import librosa as librosa
from scipy.io import wavfile
import math
import numpy as np
import matplotlib.pyplot as plt
from pydub.playback import play
from pydub import AudioSegment


class Oscillator(ABC):
    def __init__(self, freq=440, phase=0, amp=1, sample_rate=44100, wave_range=(-1, 1)):
        self._freq = freq
        self._phase = phase
        self._amp = amp
        self._sample_rate = sample_rate
        self._wave_range = wave_range

        self._f = freq
        self._a = amp
        self._p = phase

    @property
    def init_freq(self):
        return self._freq

    @property
    def init_amp(self):
        return self._amp

    @property
    def init_phase(self):
        return self._phase

    @property
    def freq(self):
        return self._f

    @freq.setter
    def freq(self, value):
        self._f = value
        self._post_freq_set()

    @property
    def amp(self):
        return self._a

    @amp.setter
    def amp(self, value):
        self._a = value
        self._post_amp_set()

    @property
    def phase(self):
        return self._p

    @phase.setter
    def phase(self, value):
        self._p = value
        self._post_phase_set()

    def _post_freq_set(self):
        pass

    def _post_amp_set(self):
        pass

    def _post_phase_set(self):
        pass

    @abstractmethod
    def _initialize_osc(self):
        pass

    @staticmethod
    def squish_val(val, min_val=0, max_val=1):
        return (((val + 1) / 2) * (max_val - min_val)) + min_val

    @abstractmethod
    def __next__(self):
        return None

    def __iter__(self):
        self.freq = self._freq
        self.phase = self._phase
        self.amp = self._amp
        self._initialize_osc()
        return self


class SineOscillator(Oscillator):
    def _post_freq_set(self):
        self._step = (2 * math.pi * self._f) / self._sample_rate

    def _post_phase_set(self):
        self._p = (self._p / 360) * 2 * math.pi

    def _initialize_osc(self):
        self._i = 0

    def __next__(self):
        val = math.sin(self._i + self._p)
        self._i = self._i + self._step
        if self._wave_range != (-1, 1):
            val = self.squish_val(val, *self._wave_range)
        return val * self._a


class WaveAdder:
    def __init__(self, *oscillators):
        self.oscillators = oscillators
        self.n = len(oscillators)

    def __iter__(self):
        [iter(osc) for osc in self.oscillators]
        return self

    def __next__(self):
        return sum(next(osc) for osc in self.oscillators) / self.n


class Synthesizer:

    def __init__(self, oscillator: Oscillator, sample_rate: int):
        self.oscillator = oscillator
        self.sample_rate = sample_rate
        self.voice = []

    def create_voice(self, notes, tempo=160, amplitude=0.1):
        samples = []
        note_duration = 60 / tempo
        wave = iter(self.oscillator)

        for note in notes:
            if type(note) is str:
                wave.freq = librosa.note_to_hz(note)
            else:
                wave.freq = note

            for _ in range(int(self.sample_rate * note_duration)):
                samples.append(next(wave))

        samples = np.array(samples) * amplitude
        return (2 ** 15 * samples).astype(np.int16)

    def add_voice(self, notes, tempo=160, amplitude=0.1):
        self.voice.append(self.create_voice(notes, tempo, amplitude))

    def play_voices(self):
        voices = []
        for voice in self.voice:
            voices.append(np.array(voice))

        wav = sum(voices)
        wavfile.write('temp.wav', self.sample_rate, wav)
        play(AudioSegment.from_wav('temp.wav'))


if __name__ == '__main__':
    osc = SineOscillator()
    synt = Synthesizer(osc, 44100)
    synt.add_voice(["C4", "E4", "G4"], amplitude=0.5)
    synt.add_voice(["E4", "G4", "C5"], amplitude=0.5)
    synt.play_voices()
