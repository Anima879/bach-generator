import numpy as np
import tensorflow as tf
import pandas as pd
from pathlib import Path
from synthesizer import Synthesizer, SineOscillator


class BachDataset:
    def __init__(self, path_jsb_dir):
        jsb_dir = Path(path_jsb_dir)
        train_files = sorted(jsb_dir.glob("train/chorale_*.csv"))
        valid_files = sorted(jsb_dir.glob("valid/chorale_*.csv"))
        test_files = sorted(jsb_dir.glob("test/chorale_*.csv"))

        self.train_chorales = BachDataset.load_chorales(train_files)
        self.valid_chorales = BachDataset.load_chorales(valid_files)
        self.test_chorales = BachDataset.load_chorales(test_files)

        self.train_set = BachDataset.bach_dataset(self.train_chorales, shuffle_buffer_size=1000)
        self.valid_set = BachDataset.bach_dataset(self.valid_chorales)
        self.test_set = BachDataset.bach_dataset(self.test_chorales)

        notes = set()
        for chorales in (self.train_chorales, self.valid_chorales, self.test_chorales):
            for chorale in chorales:
                for chord in chorale:
                    notes |= set(chord)

        self.n_notes = len(notes)
        self.min_note = min(notes - {0})
        self.max_note = max(notes)

        assert self.min_note == 36
        assert self.max_note == 81

    @staticmethod
    def create_target(batch):
        X = batch[:, :-1]
        Y = batch[:, 1:]  # predict next note in each arpegio, at each step
        return X, Y

    @staticmethod
    def preprocess(window):
        min_note = 36
        window = tf.where(window == 0, window, window - min_note + 1)  # shift values
        return tf.reshape(window, [-1])

    @staticmethod
    def load_chorales(filepaths):
        return [pd.read_csv(filepath).values.tolist() for filepath in filepaths]

    @staticmethod
    def notes_to_frequencies(notes):
        # Frequency doubles when you go up one octave; there are 12 semi-tones
        # per octave; Note A on octave 4 is 440 Hz, and it is note number 69.
        return 2 ** ((np.array(notes) - 69) / 12) * 440

    @staticmethod
    def bach_dataset(chorales: list, batch_size=32, shuffle_buffer_size=None, window_size=32, window_shift=16,
                     cache=True):
        def batch_window(window):
            return window.batch(window_size + 1)

        def to_windows(chorale):
            dataset = tf.data.Dataset.from_tensor_slices(chorale)
            dataset = dataset.window(window_size + 1, window_shift, drop_remainder=True)
            return dataset.flat_map(batch_window)

        chorales = tf.ragged.constant(chorales, ragged_rank=1)
        dataset = tf.data.Dataset.from_tensor_slices(chorales)
        dataset = dataset.flat_map(to_windows).map(BachDataset.preprocess)

        if cache:
            dataset = dataset.cache()
        if shuffle_buffer_size:
            dataset = dataset.shuffle(shuffle_buffer_size)
        dataset = dataset.batch(batch_size)
        dataset = dataset.map(BachDataset.create_target)
        return dataset.prefetch(1)


if __name__ == '__main__':

    bach_dataset = BachDataset("bach_dataset")

    chorale = bach_dataset.train_chorales[0]
    melodies = BachDataset.notes_to_frequencies(chorale).T

    osc = SineOscillator()
    synt = Synthesizer(osc, 44100)

    for melody in melodies:
        synt.add_voice(melody)

    synt.play_voices()
